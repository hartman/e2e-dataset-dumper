#ifndef SINGLEBTAG_CONFIG_HH
#define SINGLEBTAG_CONFIG_HH


#include <nlohmann/json_fwd.hpp>

#include <filesystem>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <optional>

typedef std::map<std::string,std::vector<std::string>> VariableList;

struct TrackConfig {
  std::string track_collection;
  VariableList variables;
};

std::vector<std::string> get(const VariableList&, const std::string&);
const nlohmann::ordered_json get_merged_json(const std::filesystem::path& cfg_path);
TrackConfig get_track_config(const std::filesystem::path& cfg);
TrackConfig get_track_config(const nlohmann::ordered_json& nlocfg,
                             const std::string& tool_prefix);

#endif
