#include "TrackWriterUtils.hh"
#include "TrackConfig.hh"
#include "HDF5Utils/Writer.h"

#include "xAODTracking/TrackParticle.h"
#include "xAODEventInfo/EventInfo.h"

#include <regex>

template<typename I, typename O = I>
void add_track_fillers(TrackConsumers&,
                     const std::vector<std::string>&,
                     O default_value,
                     H5Utils::Compression compression);

namespace {
  void extract_and_add_float(TrackConsumers& fillers,
                             std::vector<std::string>& var_list,
                             std::function<float(const xAOD::TrackParticle&)> fillfunc,
                             const std::string& name,
                             H5Utils::Compression compression) {
    auto itr = std::find(var_list.begin(), var_list.end(), name);
    if (itr == var_list.end()) return;

    std::function filler = [fillfunc](const TrackOutputs& jo) -> float {
      if (!jo.track) return NAN;
      return fillfunc(*jo.track);
    };
    fillers.add(name, filler, NAN, compression);
    var_list.erase(itr);
  }

  void add_track_floats(TrackConsumers& fillers,
                      std::vector<std::string> unused_track_floats,
                      H5Utils::Compression c)
  {
    extract_and_add_float(
      fillers, unused_track_floats,
      [](const auto& t){ return t.pt(); }, "pt", c);
    extract_and_add_float(
      fillers, unused_track_floats,
      [](const auto& t){ return t.eta(); }, "eta", c);
    extract_and_add_float(
      fillers, unused_track_floats,
      [](const auto& t){ return std::abs(t.eta()); }, "abs_eta", c);
    extract_and_add_float(
      fillers, unused_track_floats,
      [](const auto& t){ return t.e(); }, "energy", c);
    extract_and_add_float(
      fillers, unused_track_floats,
      [](const auto& t){ return t.m(); }, "mass", c);
    if (unused_track_floats.size() > 0) {
      add_track_fillers<float>(fillers, unused_track_floats, NAN, c);
    }
  }

}

TrackOutputs::TrackOutputs():
  track(nullptr),
  event_info(nullptr)
{}

void add_track_variables(TrackConsumers& fillers,
                         const TrackConfig& cfg,
                         H5Utils::Compression h) {
  H5Utils::Compression s = H5Utils::Compression::STANDARD;

  std::vector<std::string> track_float_variables = get(cfg.variables, "floats");
  std::vector<std::string> track_half_variables = get(cfg.variables, "halves");
  std::vector<std::string> track_char_variables = get(cfg.variables,"chars");;
  std::vector<std::string> track_uchar_variables = get(cfg.variables,"uchars");;
  std::vector<std::string> track_int_variables = get(cfg.variables,"ints");;

  add_track_floats(fillers, track_float_variables, s);
  add_track_floats(fillers, track_half_variables, h);
  add_track_fillers<char>(fillers, track_char_variables, 0, s);
  add_track_fillers<u_char>(fillers, track_uchar_variables, 0, s);
  add_track_int_variables(fillers, track_int_variables);
}


void add_track_int_variables(TrackConsumers& vars,
                           const std::vector<std::string>& names) {
  for (const auto& label_name: names) {
    SG::AuxElement::ConstAccessor<int> lab(label_name);
    std::function<int(const TrackOutputs&)> filler
      = [lab](const TrackOutputs& jo) {
          if (!jo.track) return -1;
          return lab(*jo.track);
        };
    vars.add(label_name, filler);
  }
}


void add_event_info(
  TrackConsumers& vars,
  const std::vector<std::string>& keys,
  H5Utils::Compression compression = H5Utils::Compression::STANDARD) {
  std::function<float(const TrackOutputs&)> evt_weight = [](const TrackOutputs& jo) {
    if (!jo.event_info) throw std::logic_error("missing event info");
    return jo.event_info->mcEventWeight();
  };
  std::function<long long(const TrackOutputs&)> evt_number = [](const TrackOutputs& jo) {
    if (!jo.event_info) throw std::logic_error("missing event info");
    return jo.event_info->eventNumber();
  };

  std::function<float(const TrackOutputs&)> evt_mu = [](const TrackOutputs& jo) {
    if (!jo.event_info) throw std::logic_error("missing event info");
    return jo.event_info->averageInteractionsPerCrossing();
  };

  std::function<float(const TrackOutputs&)> evt_act_mu = [](const TrackOutputs& jo) {
    if (!jo.event_info) throw std::logic_error("missing event info");
    return jo.event_info->actualInteractionsPerCrossing();
  };
  std::regex int_type("^n[A-Z].*");

  for (const auto& key: keys) {
    if (key == "mcEventWeight") {
      vars.add(key, evt_weight, NAN, compression);
    } else if (key == "eventNumber") {
      vars.add(key, evt_number);
    } else if (key == "averageInteractionsPerCrossing") {
      vars.add(key, evt_mu, NAN, compression);
    } else if (key == "actualInteractionsPerCrossing") {
      vars.add(key, evt_act_mu, NAN, compression);
    } else if (std::regex_match(key, int_type)) {
      using IA = SG::AuxElement::ConstAccessor<int>;
      std::function getter([g=IA(key)] (const TrackOutputs& jo) {
        return g(*jo.event_info);
      });
      vars.add(key, getter);
    } else {
      // let's just pretend everything else is a float
      using FA = SG::AuxElement::ConstAccessor<float>;
      std::function getter([g=FA(key)] (const TrackOutputs& jo) {
        return g(*jo.event_info);
      });
      vars.add(key, getter, NAN, compression);
    }
  }
}



TrackOutputWriter::TrackOutputWriter(H5::Group& file,
                                 const std::string& name,
                                 const TrackConsumers& cons):
  m_writer(file, name, cons)
{
}
void TrackOutputWriter::fill(const std::vector<TrackOutputs>& jos) {
  for (const auto& jo: jos) {
    m_writer.fill(jo);
  }
}
void TrackOutputWriter::flush() {
  m_writer.flush();
}


TrackOutputWriter1D::TrackOutputWriter1D(H5::Group& file,
                                     const std::string& name,
                                     const TrackConsumers& cons,
                                     size_t n_tracks):
  m_writer(file, name, cons, {n_tracks})
{
}
void TrackOutputWriter1D::fill(const std::vector<TrackOutputs>& jos) {
  m_writer.fill(jos);
}
void TrackOutputWriter1D::flush() {
  m_writer.flush();
}

// template implementation
template<typename I, typename O>
void add_track_fillers(TrackConsumers& vars,
                     const std::vector<std::string>& names,
                     O default_value,
                     H5Utils::Compression compression) {
  for (const auto& var: names) {
    SG::AuxElement::ConstAccessor<I> getter(var);
    std::function<O(const TrackOutputs&)> filler = [
      getter, default_value](const TrackOutputs& jo) -> O {
      if (!jo.track) return default_value;
      return getter(*jo.track);
    };
    vars.add(var, filler, default_value, compression);
  }
}

void add_track_float_fillers(
  TrackConsumers& c,
  const std::vector<std::string>& n,
  float default_value,
  H5Utils::Compression comp)
{
  add_track_fillers<float>(c, n, default_value, comp);
}

