#include "TrackTools.hh"
#include "TrackConfig.hh"

TrackTools::TrackTools(const TrackConfig& jobcfg):
  trkDecorator("tdd_"),
  trkTruthDecorator("tdd_")
{
  (void) jobcfg;
}

TrackTools::~TrackTools() = default;
