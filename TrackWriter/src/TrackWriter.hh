#ifndef TRACK_WRITER_HH
#define TRACK_WRITER_HH

namespace H5 {
  class Group;
}
namespace xAOD {
  class TrackParticle_v1;
  typedef TrackParticle_v1 TrackParticle;
  class EventInfo_v1;
  typedef EventInfo_v1 EventInfo;
}

class ITrackOutputWriter;
class TrackOutputs;
class TrackConfig;

#include <string>
#include <vector>
#include <map>
#include <memory>

namespace H5Utils {
  template<size_t N, typename T> class Writer;
}

class TrackWriter
{
public:
  TrackWriter(const TrackConfig&, H5::Group& output_file);
  ~TrackWriter();
  void write(const std::vector<const xAOD::TrackParticle*> tracks,
             const xAOD::EventInfo* = 0);
private:
  std::unique_ptr<ITrackOutputWriter> m_hdf5_track_writer;
  std::unique_ptr<H5Utils::Writer<0, const TrackOutputs&>> m_event_writer;
};


#endif
