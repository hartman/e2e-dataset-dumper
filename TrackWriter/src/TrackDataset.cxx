#include "TrackDataset.hh"
#include "TrackConfig.hh"

TrackDataset::TrackDataset(const TrackConfig& cfg, 
                           H5::Group& output):
  track_writer(cfg, output) {}

TrackDataset::~TrackDataset() = default;