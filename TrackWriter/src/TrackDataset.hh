#include "TrackWriter.hh"

class TrackConfig;

namespace H5 {
  class H5File;
}

struct TrackDataset {
  TrackDataset(const TrackConfig&, H5::Group&);
  ~TrackDataset();

  TrackWriter track_writer;
};