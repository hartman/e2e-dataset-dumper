#ifndef TRACK_DECORATOR_HH
#define TRACK_DECORATOR_HH

#include "xAODTracking/TrackParticle.h"
#include "xAODTracking/TrackParticleContainer.h"

#include <string>

// this is a bare-bones example of a class that could be used to
// decorate a track with additional information

class TrackDecorator
{
public:

  TrackDecorator(const std::string& decorator_prefix = "tdd_");

  // this is the function that actually does the decoration
  void decorate(const xAOD::TrackParticle& track) const;

private:
  // All this class does is apply a decoration, so all it needs to do
  // is contain one decorator. We could have also written a function
  // and statically initalized this, but static data in functions is
  // probably best avoided.

  // track params
  SG::AuxElement::Decorator<float> m_deco_d0;
  SG::AuxElement::Decorator<float> m_deco_z0;
  SG::AuxElement::Decorator<float> m_deco_phi0;
  SG::AuxElement::Decorator<float> m_deco_theta;
  SG::AuxElement::Decorator<float> m_deco_qOverP;

  SG::AuxElement::Decorator<float> m_deco_d0_err;
  SG::AuxElement::Decorator<float> m_deco_z0_err;
  SG::AuxElement::Decorator<float> m_deco_phi0_err;
  SG::AuxElement::Decorator<float> m_deco_theta_err;
  SG::AuxElement::Decorator<float> m_deco_qOverP_err;

};

#endif
