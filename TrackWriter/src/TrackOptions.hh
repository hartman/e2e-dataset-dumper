#ifndef TRACK_OPTIONS_HH
#define TRACK_OPTIONS_HH

#include <vector>
#include <string>
#include <filesystem>

struct TrackIOOpts
{
  std::vector<std::string> in;
  std::string out;
  size_t max_events;
  std::filesystem::path config_file_name;
};

TrackIOOpts get_track_io_opts(int argc, char* argv[]);


#endif