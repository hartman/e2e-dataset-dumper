#ifndef PROCESS_TRACKS_HH
#define PROCESS_TRACKS_HH

class TrackDataset;
class TrackConfig;
class TrackTools;

namespace xAOD {
  class TEvent;
  class EventInfo_v1;
  typedef EventInfo_v1 EventInfo;
  class TrackParticle_v1;
  typedef TrackParticle_v1 TrackParticle;
}

void processTracks(xAOD::TEvent&, 
                   const TrackConfig&,
                   const TrackTools&,
                   TrackDataset&);


#endif  // PROCESS_TRACKS_HH
