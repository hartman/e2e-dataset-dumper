#include "TrackWriter.hh"
#include "TrackConfig.hh"
#include "TrackWriterUtils.hh"

#include "HDF5Utils/Writer.h"

// Less Standard Libraries (for atlas)
#include "H5Cpp.h"

// ATLAS things
#include "xAODEventInfo/EventInfo.h"

TrackWriter::TrackWriter(const TrackConfig& cfg,
                         H5::Group& output_file)
{
  using H5Utils::Compression;

  Compression f = Compression::STANDARD;

  // create the variable fillers
  TrackConsumers fillers;

  add_track_variables(fillers, cfg, f);

  // now create the writer
  m_hdf5_track_writer.reset(
    new TrackOutputWriter1D(
      output_file, "tracks", fillers, 500));
  // we create another writer for the event info if we're writing
  // tracks in a per-event way.
  TrackConsumers event_fillers;

  std::vector<std::string> event_info{"mcEventWeight",
                                      "eventNumber",
                                      "averageInteractionsPerCrossing",
                                      "actualInteractionsPerCrossing"
                                      };
  add_event_info(event_fillers, event_info, f);
  
  m_event_writer.reset(
    new H5Utils::Writer<0,const TrackOutputs&>(
      output_file, "events", event_fillers));
}

TrackWriter::~TrackWriter() {
  if (m_hdf5_track_writer) m_hdf5_track_writer->flush();
}

void TrackWriter::write(const std::vector<const xAOD::TrackParticle*> tracks,
                          const xAOD::EventInfo* mc_event_info) {
  std::vector<TrackOutputs> jos;
  for (const auto* track: tracks) {
    TrackOutputs jo;
    jo.event_info = mc_event_info;
    jo.track = track;
    jos.push_back(jo);
  }
  m_hdf5_track_writer->fill(jos);
  if (m_event_writer) {
    TrackOutputs jo;
    jo.event_info = mc_event_info;
    m_event_writer->fill(jo);
  }
}
