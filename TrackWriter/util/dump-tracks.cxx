#include "processTracks.hh"
#include "TrackOptions.hh"
#include "TrackConfig.hh"
#include "TrackDataset.hh"
#include "TrackTools.hh"

#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"

#include "H5Cpp.h"

#include "TFile.h"
#include "TTree.h"

namespace {
  void check_rc(StatusCode code) {
    if (!code.isSuccess()) throw std::runtime_error("bad return code");
  }
}

int main (int argc, char *argv[]) {
  
  TrackIOOpts opts = get_track_io_opts(argc, argv);
  TrackConfig jobcfg = get_track_config(opts.config_file_name);

  // The name of the application:
  const char *const APP_NAME = "TrackDumper";

  // Set up the environment:
  check_rc( xAOD::Init() );

  // Set up the event object:
  xAOD::TEvent event(xAOD::TEvent::kClassAccess);

  H5::H5File output_file(opts.out, H5F_ACC_TRUNC);
  TrackDataset out(jobcfg, output_file);
  TrackTools tools(jobcfg);


  // Loop over the specified files:
  for (const std::string& file: opts.in) {
    // Open the file:
    std::unique_ptr<TFile> ifile(TFile::Open(file.c_str(), "READ"));
    if ( ! ifile.get() || ifile->IsZombie()) {
      Error( APP_NAME, "Couldn't open file: %s", file.c_str() );
      return 1;
    }
    Info( APP_NAME, "Opened file: %s", file.c_str() );

    // Connect the event object to it:
    check_rc( event.readFrom(ifile.get()) );

    // Loop over its events:
    size_t entries = event.getEntries();
    if (opts.max_events > 0) entries = std::min(opts.max_events, entries);
    for (size_t entry = 0; entry < entries; ++entry) {

      // Load the event:
      if (event.getEntry(entry) < 0) {
        Error( APP_NAME, "Couldn't load entry %zu from file: %s",
               entry, file.c_str() );
        return 1;
      }

      // Print some status:
      if ( ! (entry % 500) ) {
        Info( APP_NAME, "Processing event %zu / %zu", entry, entries );
      }

      // most of the real work happens in this function
      processTracks(event, jobcfg, tools, out);

    } // end event loop

  } // end file loop

  Info( APP_NAME, "Successful run.");

  return 0;
}
